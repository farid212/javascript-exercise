## Exercise: Fair price

Declare the following variables:

* Variable that will be used to display a message
* Variable that will count the number of tests
* Variable of the **minimum** value that can be entered (here 20)
* Variable of the **maximum** value that can be entered (here 80)

Declare the following functions:

* creates a function
    - Function that returns a random number between **minimum** and the **maximum** variable, rounded to the nearest integer
    - Function that takes an argument and test if the number entered matches the random number generated above. Returns one of the following strings based on the test result:  `It's more, It's less, It's just you found in X moves`

Use the following console **or** native features:

window.prompt ();
window.alert ();
window.confirm ();